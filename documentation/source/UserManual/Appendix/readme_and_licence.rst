.. _readme:

Appendix E: DL_POLY_4 INSTALL Notes & README Wisdom
===================================================

BUILDING 
--------

.. include:: ../../../../building.md 

CODING STYLE 
------------

.. include:: ../../../../coding_style.md 

CONTRIBUTING 
------------

.. include:: ../../../../contributing.md
    

LICENCE
-------

.. include:: ../../../../LICENCE.md